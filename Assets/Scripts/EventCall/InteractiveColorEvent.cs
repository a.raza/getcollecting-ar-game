﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveColorEvent : MonoBehaviour
{
    #region Private Variable

    [SerializeField] private Color[] colors;
    [SerializeField] private Image[] images2Reset;
    private Image img;

    #endregion

    #region Event Calls

    /// <summary>
    /// Will register the image before applying the color
    /// </summary>
    /// <param name="_img"></param>
    public void ImageRegister(Image _img)
    {
        img = _img;
    }

    /// <summary>
    /// Will set color of the button image, when the button gets pressed
    /// </summary>
    public void SetImageColor()
    {
        if (img == null)
            return;

        if (img.color == colors[1])
            Reset();
        else
        {
            Reset();
            img.color = colors[1];
        }
    }

    #endregion

    #region Supporting Functions
    /// <summary>
    /// Will reset the color of all registered images
    /// </summary>
    private void Reset()
    {
        foreach(var display in images2Reset)
        {
            display.color = colors[0];
        }
    }

    #endregion
}
