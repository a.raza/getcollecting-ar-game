﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehavior : MonoBehaviour
{
	#region Private Variable
	[SerializeField] private float m_speed;
	[SerializeField] private Controller controller;
	[SerializeField] private Rigidbody rigidbody;
	private Vector3 targetDir, dir;
    #endregion

    #region Unity Callbacks
    private void Update()
	{
		Controls();     // allow to use controller	
	}
	#endregion

	#region Support Functions
	private void Controls()
	{
		// if controller is in use.. // and not in the state of get hit
		if (Is_usingControls())
		{
			//allow protagonist to rotate
			Locomote();

			//allow protagonist to move
			Move();
		}
	}

	private bool Is_usingControls()
	{
		return controller.GetPosition() != Vector3.zero;
	}

	//** following vector wil be the virtual position which will always be one step ahead of the position of the Protagonist,
	// making the movement of the character more smooth and realistic
	private Vector3 VirtualPosition()
	{
		return new Vector3
			(
				//getting the current x axis of the protagonist, and adding +1 to the x axis.. returns one step a head of the current position of the protagonist
				rigidbody.velocity.x + controller.GetPosition().x
				,
				//y axis will alwasy be 0, as the only directions here that are necessary are: Forward, Backward, Left and Right
				0
				,
				//getting the current z axis of the protagonist, and adding +1 to the z axis.. returns one step a head of the current position of the protagonist
				rigidbody.velocity.z + controller.GetPosition().z
			);
	}

	//following method is being used for rotating the protagonist
	private void Locomote()
	{
		//getting the distance between the virtual position(Whic is one step a head of the current position of the protagonist) and the current position of the protagonsit
		targetDir = VirtualPosition() - rigidbody.velocity;

		//only forward, backward, left and right direction are required here
		dir = new Vector3(targetDir.x, 0, targetDir.z);

		// This will rotate the character according to the controller
		transform.rotation = Quaternion.LerpUnclamped
			(
				transform.rotation, // get the current rotaiont of the protagonist
				Quaternion.LookRotation(dir), // make protagonist look towards the direction
				m_speed * Time.deltaTime    // giving it a speed to make a smooth turn/rotation
			);
	}

	private void Move()
	{
		// This will move the character according to the controller
		transform.position += new Vector3
			(
				(controller.GetPosition().x * m_speed * Time.deltaTime), //updating horizontal movement
				0,
				(controller.GetPosition().z * m_speed * Time.deltaTime) //updating Vertical movement
			);
	}
    #endregion
}
