﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class JoystickOperator : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    #region Private Variable
    [SerializeField] private Image bgJoyStick, joystick;
    [SerializeField] private float joystickOffset;

    private Vector3 controllerInput;
    #endregion

    #region Unity Callbacks
    private void Start() => IsValid();
    #endregion

    #region Error Check
    public void IsValid()
    {
        if (bgJoyStick == null || joystick == null || controllerInput == null)
        {
            Debug.LogError(@"JoystickOperator Not Operational!");
            this.gameObject.SetActive(false);
        }
    }
    #endregion

    #region Event Calls
    public void OnDrag(PointerEventData eventData)
    {
        Vector2 pos;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgJoyStick.rectTransform,
            eventData.position,
            eventData.pressEventCamera,
            out pos))
        {
            var delta = bgJoyStick.rectTransform.sizeDelta;

            pos.x /= delta.x;
            pos.y /= delta.y;

            controllerInput = new Vector3
                (
                    pos.x * joystickOffset,
                    pos.y * joystickOffset
                );

            controllerInput = (controllerInput.magnitude > 1.0f) ? controllerInput.normalized : controllerInput;
            joystick.rectTransform.anchoredPosition = new Vector3
                (
                    controllerInput.x * (delta.x / joystickOffset),
                    controllerInput.y * (delta.y / joystickOffset)
                );
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OnDrag(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        controllerInput = Vector3.zero;
        joystick.rectTransform.anchoredPosition = Vector3.zero;
    }
    #endregion

    #region Support Functions
    public bool IsControllerInUse()
    {
        return controllerInput != Vector3.zero;
    }

    public Vector3 Controller()
    {
        return controllerInput;
    }
    #endregion
}