﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class GameBoardPlacement : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private GameObject gameBoard;
    private ARRaycastManager raycastManager;
    private Vector2 position;
    private Transform target;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    #endregion


    #region Unity Callbacks

    private void Awake()
    {
        raycastManager = GetComponent<ARRaycastManager>();
    }

    private void Update()
    {
        RepositionOperation();
    }

    #endregion

    #region Touch Support
    private bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    private void RepositionOperation()
    {
        if (!TryGetTouchPosition(out position))
            return;

        if (raycastManager.Raycast(position, hits, TrackableType.PlaneWithinPolygon))
        {
            Debug.Log("Position: " + position);

            var hitPose = hits[0].pose;

            if (target == null)
            {
                target = Instantiate(gameBoard, hitPose.position, hitPose.rotation).transform;
            }
            else
            {
                target.position = hitPose.position;
            }
        }
    }
    #endregion
}
