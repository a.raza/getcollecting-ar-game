﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    #region Private Variable
    private float xaxis, zaxis;
    private Vector3 postion;
    private Transform cam;
    [SerializeField] private JoystickOperator joystick;
    #endregion

    #region Unity Callbacks
    private void Start()
    {
        cam = Camera.main.transform;
        postion = Vector3.zero;
    }

    void Update()
    {
        if (cam == null)
        {
            Start();
            return;
        }

        postion = Position();
    }
    #endregion

    #region Controller Support
    private Vector3 Position()
    {
        xaxis = joystick.Controller().x;
        zaxis = joystick.Controller().z;

        // if player is using the controls...
        if (joystick.IsControllerInUse())
        {
            Vector3 forward = cam.TransformDirection(Vector3.forward);

            // Player is moving on ground, Y component of camera facing is not relevant.
            forward.y = 0.0f;
            forward = forward.normalized;

            // Calculate target direction based on camera forward and direction key.
            Vector3 right = new Vector3(forward.z, 0, -forward.x);
            Vector3 targetDirection;
            targetDirection = forward * zaxis + right * xaxis;

            return targetDirection;
        }
        else
        {
            return Vector3.zero;
        }
    }

    public Vector3 GetPosition()
    {
        return postion;
    }
    #endregion
}