﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CharacterRegistration  : MonoBehaviour
{
    #region Private Variable

    private const float DURATION = 0.1f;

    private int
        index = 0,
        limit,
        indexLimit;

    [SerializeField] private Button next, back;
    [SerializeField] private Animator transition;
    [SerializeField] private Transform root;
    [SerializeField] private float interval;
    [SerializeField] private GameObject[] charactersPrefabs;

    #endregion

    #region Protected Variable

    protected GameObject[] characters;
    protected GameObject currentCharacter;

    #endregion

    #region Unity Callbacks

    protected virtual void Start()
    {
        PrepareInteractables();
        Register();
        Next();
    }

    #endregion

    #region Supporting Functions
    /// <summary>
    /// Will apply Event calls to the interactables
    /// </summary>
    private void PrepareInteractables()
    {
        next.onClick.AddListener(() => { Next(); });
        back.onClick.AddListener(() => { Back(); });
    }

    /// <summary>
    /// Will create and store all the playable characters
    /// </summary>
    private void Register()
    {
        limit = charactersPrefabs.Length;
        indexLimit = limit - 1;
        characters = new GameObject[limit];

        for (int i = 0; i < limit; i++)
        {
            characters[i] = Instantiate(charactersPrefabs[i], root);
            var character = characters[i].transform;
            character.localPosition = Vector3.zero;
            character.localRotation = Quaternion.Euler(Vector3.zero);
            character.localScale = Vector3.zero;
            characters[i].SetActive(false);
        }

        currentCharacter = characters[index];
        currentCharacter.SetActive(true);
    }


    /// <summary>
    /// Will do overall process of Transition
    /// </summary>
    private void DoScale()
    {
        //Disable current Character
        currentCharacter.transform.localScale = Vector3.zero;
        currentCharacter.SetActive(false);

        //Play Transition
        transition.Play(0);

        //Get next/previous Character
        currentCharacter = characters[index];
        currentCharacter.SetActive(true);

        //Do Character Appearing Animation
        var sequence = DOTween.Sequence().SetAutoKill(true);
        sequence.AppendInterval(interval);
        sequence.Append(currentCharacter.transform.DOLocalMove(Vector3.zero, DURATION));
        sequence.Append(currentCharacter.transform.DOLocalRotate(Vector3.zero, DURATION));
        sequence.Append(currentCharacter.transform.DOScale(Vector3.one, DURATION));
    }

    /// <summary>
    /// Will Active/Deactive Next/Back accordingly
    /// </summary>
    private void InteractiveBehaviour()
    {
        next.gameObject.SetActive(index != indexLimit);
        back.gameObject.SetActive(index != 0);
    }

    #endregion


    #region Event Calls

    /// <summary>
    /// Get the next available character
    /// </summary>
    public void Next()
    {
        index++;

        //Disable controls

        if (index >= limit)
        {
            index = indexLimit;
            return;
        }

        DoScale();
        InteractiveBehaviour();
    }

    /// <summary>
    /// Get the previous available character
    /// </summary>
    public void Back()
    {
        index--;

        //Disable controls

        if (index < 0)
        {
            index = 0;
            return;
        }

        DoScale();
        InteractiveBehaviour();
    }

    #endregion
}