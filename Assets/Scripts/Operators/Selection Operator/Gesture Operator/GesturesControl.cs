﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GesturesControl : CharacterRegistration
{
    #region Enum

    public enum Operations
    {
        NONE = 0,
        SCALE = 1,
        ROTATION = 2,
        PAN = 3,
    };

    #endregion

    #region Private Variable

    private Operations operation;

    [Header("Pan Settings")]
    [SerializeField] private float minY;
    [SerializeField] private float maxY;
    [SerializeField] private float panSensitivity;

    [Header("Scale Settings")]
    [SerializeField] private float minScale;
    [SerializeField] private float maxScale;
    [SerializeField] private float scaleSensitivity;

    [Header("Rotation Settings")]
    [SerializeField] private float rotAngleMinimum;
    [SerializeField] private float rotationSensitivity;

    private Vector2 rotationStart;
    private Vector3 panStart;
    private float rotationY;

    #endregion

    #region Unity Callbacks

    private void Awake()
    {
        operation = Operations.NONE;
    }

    protected override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        //Debug.Log(string.Format("Current Operation: {0}", operation.ToString()));
        switch (operation)
        {
            case Operations.PAN:
                PanOperation();
                break;
            case Operations.SCALE:
                ScaleOperation();
                break;
            case Operations.ROTATION:
                RotationOperation();
                break;
        }
    }

    #endregion

    #region Gestures Operations

    private void PanOperation()
    {
        if (currentCharacter == null)
            return;

        if (Input.touchCount == 1)
        {
            var touch0 = Input.GetTouch(0);

            var target = currentCharacter.transform;

            switch (touch0.phase)
            {
                case TouchPhase.Began:
                case TouchPhase.Stationary:
                    panStart = touch0.position;
                    break;
                case TouchPhase.Moved:
                    Vector2 direction = (Vector2)panStart - touch0.position;
                    var clampedValue = Mathf.Clamp(target.localPosition.y - (direction.y * panSensitivity * Time.smoothDeltaTime), minY, maxY);
                    target.localPosition = new Vector3(0, clampedValue, target.localPosition.z);
                    break;
                case TouchPhase.Ended:
                    break;
            }
        }
    }

    private void ScaleOperation()
    {
        if (currentCharacter == null)
            return;

        if (Input.touchCount == 2)
        {
            var touch0 = Input.GetTouch(0);
            var touch1 = Input.GetTouch(1);

            switch (touch1.phase)
            {
                case TouchPhase.Moved:

                    var previousPosition0 = touch0.position - touch0.deltaPosition;
                    var previousPosition1 = touch1.position - touch1.deltaPosition;

                    var previousdMagnitude = (previousPosition0 - previousPosition1).magnitude;
                    var currentMagnitude = (touch0.position - touch1.position).magnitude;

                    var outcome = currentMagnitude - previousdMagnitude;

                    Scale(outcome * (scaleSensitivity * Time.smoothDeltaTime));

                    break;

                case TouchPhase.Stationary:
                case TouchPhase.Ended:
                    break;
            }
        }
    }

    /// <summary>
    /// Will give an illusion of resizing the character
    /// NOTO: The character is not being scaled here, rather it is being push/pull in front of the camera.
    /// Developer Statement: It is possible to edit following code to in a manner to be used to scale a GameObject
    /// </summary>
    /// <param name="value"></param>
    private void Scale(float value)
    {
        var target = currentCharacter.transform;
        var scaleValue = target.localPosition.z;
        var afterScaleValue = Mathf.Clamp(scaleValue + value, minScale, maxScale);
        target.localPosition = new Vector3(0, target.localPosition.y, afterScaleValue);
    }

    private void RotationOperation()
    {
        if (currentCharacter == null)
            return;

        if (Input.touchCount == 2)
        {
            var touch0 = Input.GetTouch(0);
            var touch1 = Input.GetTouch(1);

            switch (touch1.phase)
            {
                case TouchPhase.Began:
                case TouchPhase.Stationary:
                    rotationStart = touch1.position - touch0.position;
                    break;

                case TouchPhase.Moved:

                    var currVector = touch1.position - touch0.position;
                    var angleOffset = Vector2.Angle(rotationStart, currVector);
                    var crossProdcut = Vector3.Cross(rotationStart, currVector);
                    var target = currentCharacter.transform;

                    if (angleOffset > rotAngleMinimum)
                    {
                        if(crossProdcut.z > 0)
                            Rotate(target, true);
                        else if(crossProdcut.z < 0)
                            Rotate(target, false);
                    }
                    break;

                case TouchPhase.Ended:
                    break;
            }
        }
    }

    /// <summary>
    /// Will rotate the character in Y axis direction
    /// </summary>
    /// <param name="target"></param>
    /// <param name="isNegative"></param>
    private void Rotate(Transform target, bool isNegative)
    {
        rotationY = target.localRotation.eulerAngles.y;
        if(isNegative)
            rotationY -= (rotationSensitivity * Time.smoothDeltaTime);
        else
            rotationY += (rotationSensitivity * Time.smoothDeltaTime);
        target.localRotation = Quaternion.Euler(new Vector3(0, rotationY, 0));
    }

    #endregion

    #region Event Calls

    public void SetOperation(int value)
    {
        if(value == (int)operation)
        {
            operation = Operations.NONE;
        }
        else
        {
            operation = (Operations)value;
        }
    }
    #endregion
}