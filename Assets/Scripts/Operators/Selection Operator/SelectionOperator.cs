﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SelectionOperator : GesturesControl
{
    #region Private Variable

    private const float
        TRANSITION = 5f,
        RESIZE = 0.5f;

    [Header("Selection Settings")]
    [SerializeField] private Image transitionDisplay;
    [SerializeField] private GameObject arZoneUI;
    private Color transparent;

    #endregion

    #region Unity Callbacks

    protected override void Start()
    {
        base.Start();
        transparent = new Color(0, 0, 0, 0);
    }

    #endregion

    #region Event Calls

    public void OnSelection()
    {
        var target = currentCharacter.transform;

        var sequence = DOTween.Sequence().SetAutoKill(true);
        sequence.Append(target.transform.DOScale(Vector3.zero, RESIZE));
        sequence.Append(transitionDisplay.DOColor(transparent, TRANSITION));
        sequence.OnComplete(() =>
        {
            transitionDisplay.gameObject.SetActive(false);
            this.enabled = false;
            arZoneUI.SetActive(true);
        });
    }

    #endregion
}