﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    #region Private Variable
    [SerializeField] private Transform root;
    [SerializeField] private float
        xAxis,
        zAxis,
        spaceBetween;
    [SerializeField] private int levelSize;
    [SerializeField] private GameObject
        floor,
        collectablePrefab;
    [SerializeField] private GameObject[] blockDesigns;
    private int availableBlockDesigns;

    #endregion

    #region Protected Variable
    protected int matrix;
    protected int blocksAmount;
    protected GameObject[] designCollection;

    #endregion

    #region Unity Callbacks
    protected virtual void Start()
    {
        availableBlockDesigns = blockDesigns.Length;
        matrix = levelSize * levelSize;
        blocksAmount = (int)((matrix / 2) / 2);
        designCollection = new GameObject[blocksAmount];
        xAxis = matrix;
        zAxis = matrix;

        //GenerateFloor();
        //CoinsDistribution();
    }
    #endregion

    #region Supporting Functions
    private void Registration()
    {
        for (int i = 0; i < blocksAmount; i++)
        {
            var rand = Random.Range(0, availableBlockDesigns);
            designCollection[i] = Instantiate(blockDesigns[rand], root);
        }
    }

    private void GenerateBlocks()
    {

    }

    //private void GenerateFloor()
    //{
    //    Generate(floor, 0.1f);
    //}

    //private void CoinsDistribution()
    //{
    //    Generate(collectablePrefab, 0.1f);
    //}

    //private void Generate(GameObject prefab, float space)
    //{
    //    float
    //        currentX = xAxis,
    //        currentZ = zAxis;

    //    for (int x = 0; x < levelSize; x++)
    //    {
    //        for (int z = 0; z < levelSize; z++)
    //        {
    //            var obj = Instantiate(prefab, root).transform;
    //            obj.localPosition = new Vector3(currentX, obj.localPosition.y, currentZ);
    //            currentX += space;
    //        }

    //        currentX = xAxis;
    //        currentZ += space;
    //    }
    //}

    #endregion
}
